from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "items/create.html"
    fields = ["list", "task", "due_date", "is_completed"]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    fields = ["list", "task", "due_date", "is_completed"]

    def get_sucess_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])
